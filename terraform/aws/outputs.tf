output "public_ip" {
  value = try(data.aws_instance.this[0].public_ip, null)
}

output "instance_id" {
  value = try(data.aws_instance.this[0].id, null)
}

output "uuid" {
  value = local.uuid
}

output "ssh_known_hosts" {
  value = try(data.aws_ssm_parameter.this_known_hosts[0].value, null)
}

output "region" {
  value = var.region
}
