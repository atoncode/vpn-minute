data "aws_caller_identity" "current" {}

locals {
  ami_owners = {
    ubuntu = "099720109477"
    alpine = "538276064493"
  }

  ami_name_search = {
    ubuntu = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
    alpine = "alpine-ami-3.11*"
  }

  ami_size = {
    ubuntu = 8
    alpine = 1
  }
}

data "aws_ami" "ami" {
  for_each = local.need_defaut_ami_id ? { 0 = "enabled" } : {}

  most_recent = true
  owners      = [lookup(local.ami_owners, var.ami_os)]

  filter {
    name   = "name"
    values = [lookup(local.ami_name_search, var.ami_os)]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_vpc" "default" {
  for_each = local.need_default_vpc ? { 0 = "enabled" } : {}

  default = true
}

data "aws_subnet_ids" "this" {
  for_each = "" == var.subnet_id && local.need_default_vpc ? { 0 = "enabled" } : {}

  vpc_id = try(data.aws_vpc.default[0].id, null)
}

data "aws_instance" "this" {
  for_each = var.destroy == false ? { 0 = "enabled" } : {}

  filter {
    name   = "tag:aws:ec2spot:fleet-request-id"
    values = [aws_spot_fleet_request.this[0].id]
  }
}

data "template_file" "user_data" {
  for_each = { 0 = "enabled" }

  template = file("${path.module}/templates/${var.ami_os}_user_data.tmpl")
  vars = {
    allow_ssh                     = var.allow_ssh
    region                        = var.region
    base64_wg_server_config       = var.base64_vpn_server_config
    known_host_ssm_parameter_name = try(aws_ssm_parameter.this_known_hosts[0].name, "")
  }
}

data "aws_ssm_parameter" "this_known_hosts" {
  count = var.allow_ssh ? 1 : 0

  name = aws_ssm_parameter.this_known_hosts[0].name
}
