locals {
  need_default_vpc   = "" == var.vpc_id
  need_defaut_ami_id = "" == var.ami_id

  ssh_is_allowed = var.public_key != "" && var.allow_ssh

  prefix = "vpnm"
  uuid   = var.uuid != "" ? var.uuid : random_string.this[0].result
  name   = "${local.prefix}-${local.uuid}"

  tags = {
    managed-by  = "terraform"
    application = var.application_name
    uuid        = local.uuid
  }
}

resource "random_string" "this" {
  for_each = var.uuid == "" ? { 0 = "enabled" } : {}

  length  = 8
  special = false
  upper   = false
}

####
# Security Group
####

resource "aws_security_group" "this" {
  for_each = { 0 = "enabled" }

  name        = "sgs-${local.name}"
  description = "${var.application_name} security group for instance ${local.uuid}"
  vpc_id      = try(data.aws_vpc.default[0].id, var.vpc_id)

  tags = merge(
    local.tags,
    {
      Name = "sgs-${local.name}"
    }
  )
}

resource "aws_security_group_rule" "this_ingress" {
  type        = "ingress"
  from_port   = 51820
  to_port     = 51820
  protocol    = "udp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.this[0].id
}

resource "aws_security_group_rule" "this_ingress_22" {
  for_each = local.ssh_is_allowed ? { 0 = "enabled" } : {}

  type        = "ingress"
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.this[0].id
}

resource "aws_security_group_rule" "this_egress" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = -1
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.this[0].id
}

####
# Compute
####

resource "aws_key_pair" "this" {
  for_each = local.ssh_is_allowed ? { 0 = "enabled" } : {}

  key_name   = "ssh-${local.name}"
  public_key = var.public_key

  tags = merge(
    local.tags,
    {
      Name = "ssh-${local.name}"
    }
  )
}

resource "aws_launch_template" "this" {
  for_each = { 0 = "enabled" }

  name = local.name

  image_id      = try(data.aws_ami.ami[0].id, var.ami_id)
  instance_type = var.instance_type

  user_data = base64encode(replace(data.template_file.user_data[0].rendered, "\r\n", "\n"))

  network_interfaces {
    security_groups             = [aws_security_group.this[0].id]
    associate_public_ip_address = true
    delete_on_termination       = true
  }

  block_device_mappings {
    device_name = "/dev/sda1"

    ebs {
      delete_on_termination = true
      encrypted             = true
      volume_size           = lookup(local.ami_size, var.ami_os)
      volume_type           = "gp2"
    }
  }

  dynamic "iam_instance_profile" {
    for_each = local.ssh_is_allowed ? [1] : []

    content {
      name = aws_iam_instance_profile.this[0].name
    }
  }

  key_name = try(aws_key_pair.this[0].key_name, null)

  tag_specifications {
    resource_type = "instance"

    tags = merge(
      local.tags,
      {
        Name = "ec2-${local.name}"
      }
    )
  }

  tags = merge(
    local.tags,
    {
      Name = "ltp-${local.name}"
    }
  )

  lifecycle {
    ignore_changes = [user_data]
  }
}

resource "aws_spot_fleet_request" "this" {
  for_each = { 0 = "enabled" }

  iam_fleet_role  = aws_iam_role.this_spotfleet[0].arn
  target_capacity = 1
  valid_until     = timeadd(timestamp(), "86400h")

  terminate_instances_with_expiration = true

  launch_template_config {
    launch_template_specification {
      id      = aws_launch_template.this[0].id
      version = aws_launch_template.this[0].latest_version
    }

    overrides {
      subnet_id = tolist(try(data.aws_subnet_ids.this[0].ids, var.subnet_id))[0]
    }
  }

  lifecycle {
    ignore_changes = [valid_until]
  }

  tags = merge(
    local.tags,
    {
      Name = "sfr-${local.name}"
    }
  )
}

####
# SSM Parameter
####

resource "aws_ssm_parameter" "this_known_hosts" {
  for_each = local.ssh_is_allowed ? { 0 = "enabled" } : {}

  name        = "/${local.prefix}/${local.uuid}/known-hosts"
  description = "${var.application_name} server ${local.uuid} known hosts in base64."
  type        = "String"
  value       = "IN PROGRESS..."
  overwrite   = true

  tags = merge(
    local.tags,
    {
      Name = "ssm-${local.name}"
    }
  )

  lifecycle {
    ignore_changes = [value]
  }
}

####
# IAM Policy/Role/Instance Profile
####

data "aws_iam_policy_document" "spotfleet" {
  for_each = { 0 = "enabled" }

  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "spotfleet.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_role" "this_spotfleet" {
  for_each = { 0 = "enabled" }

  name = "rol-spf-${local.name}"

  description        = "${var.application_name} role for sfr-${local.name}."
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.spotfleet[0].json

  tags = merge(
    local.tags,
    {
      Name = "rol-spf-${local.name}"
    }
  )
}

resource "aws_iam_role_policy_attachment" "this_spotfleet" {
  for_each = { 0 = "enabled" }

  role       = aws_iam_role.this_spotfleet[0].id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole"
}

data "aws_iam_policy_document" "this" {
  for_each = local.ssh_is_allowed ? { 0 = "enabled" } : {}

  statement {
    sid = "VPNMAllowReadSSMParameterAccess"

    effect = "Allow"

    actions = [
      "ssm:GetParameter",
      "ssm:GetParameters",
      "ssm:PutParameter",
    ]

    resources = formatlist(
      "arn:aws:ssm:*:%s:parameter/%s/%s/known-hosts",
      data.aws_caller_identity.current.account_id,
      local.prefix,
      local.uuid,
    )
  }
}

data "aws_iam_policy_document" "sts_instance" {
  for_each = local.ssh_is_allowed ? { 0 = "enabled" } : {}

  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "ec2.amazonaws.com"
      ]
    }
  }
}

resource "aws_iam_policy" "this_instance_profile" {
  for_each = local.ssh_is_allowed ? { 0 = "enabled" } : {}

  name        = "plc-${local.name}"
  path        = "/"
  policy      = data.aws_iam_policy_document.this[0].json
  description = "${var.application_name} read/write policy to get access to ssm-${local.name} SSM parameters."
}

resource "aws_iam_role" "this_instance_profile" {
  for_each = local.ssh_is_allowed ? { 0 = "enabled" } : {}

  name = "rol-ipr-${local.name}"

  description        = "${var.application_name} role for ipr-${local.name} instance profile."
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.sts_instance[0].json

  tags = merge(
    local.tags,
    {
      Name = "rol-ipr-${local.name}"
    }
  )
}

resource "aws_iam_role_policy_attachment" "this_instance_profile" {
  for_each = local.ssh_is_allowed ? { 0 = "enabled" } : {}

  role       = aws_iam_role.this_instance_profile[0].id
  policy_arn = aws_iam_policy.this_instance_profile[0].arn
}

resource "aws_iam_instance_profile" "this" {
  for_each = local.ssh_is_allowed ? { 0 = "enabled" } : {}

  name = "ipr-${local.name}"
  path = "/"

  role = aws_iam_role.this_instance_profile[0].id
}
